<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $createdAt = $this->faker->dateTimeThisMonth();
        return [
            'product_name' => 'товар №_' . $this->faker->unique()->numberBetween(1, 100),
            'price' => $this->faker->numberBetween(100, 5000),
            'published' => $this->faker->boolean(),
            'created_at' => $createdAt,
            'updated_at' => $createdAt,
            'deleted_at' => $this->faker->optional(0.8)->dateTimeThisMonth(),
        ];
    }
}
