<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            if (!Schema::hasTable('products')) {
                Schema::create('products', function (Blueprint $table) {
                    $table->id();
                    $table->string('product_name')->comment('Название товара');
                    $table->unsignedInteger('price')->nullable()->comment('Цена');
                    $table->boolean('published')->default(false)->comment('Признак публикации позиции');
                    $table->timestampsTz();
                    $table->softDeletesTz();
                });
            }

            if (!Schema::hasTable('categories')) {
                Schema::create('categories', function (Blueprint $table) {
                    $table->id();
                    $table->string('category_name')->unique()->comment('Название товара');
                });
            }

            if (!Schema::hasTable('product_category') &&
                Schema::hasTable('products') &&
                Schema::hasTable('categories')) {
                    Schema::create('product_category', function (Blueprint $table) {
                        $table->id();
                        $table->foreignId('product_id')
                            ->constrained('produts')
                            ->comment('ID товара');
                        $table->foreignId('category_id')
                            ->constrained('categories')
                            ->onDelete('cascade')
                            ->comment('ID категории');
                        $table->timestampTz('created_at')->useCurrent();
                    });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('products');
    }

    /**
     * Метод необходим, т.к. транзакции с php.8.0 отрабатывают некорректно.
     * В перспективе данный метод лучше вынести в интерфейс.
     * @return bool
     */
    public function isTransactional(): bool
    {
        return false;
    }
}
