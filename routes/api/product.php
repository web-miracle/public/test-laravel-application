<?php

use App\Http\Controllers\ProductController;


Route::group(['prefix' => 'product',], function () {
    Route::get('/', [ProductController::class, 'index'])->name('product.index');
    Route::get('{id}', [ProductController::class, 'show'])->name('product.show');
    Route::post('/', [ProductController::class, 'store'])->name('product.store');
    Route::post('{id}', [ProductController::class, 'update'])->name('product.update');
    Route::delete('{id}', [ProductController::class, 'destroy'])->name('product.destroy');
    Route::post('{id}/restore', [ProductController::class, 'restore'])->name('product.restore');
});

