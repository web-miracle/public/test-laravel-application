<?php

use App\Http\Controllers\CategoryController;


Route::group(['prefix' => 'category',], function () {
    Route::get('/', [CategoryController::class, 'index'])->name('category.index');
    Route::get('{id}', [CategoryController::class, 'show'])->name('category.show');
    Route::post('/', [CategoryController::class, 'store'])->name('category.store');
    Route::put('{id}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('{id}', [CategoryController::class, 'destroy'])->name('category.destroy');
});

