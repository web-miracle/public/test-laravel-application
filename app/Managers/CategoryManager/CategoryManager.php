<?php

namespace App\Managers\CategoryManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use App\Managers\CategoryManager\Repositories\CategoryRepository;
use App\Models\Category;

class CategoryManager {

    /** @var CategoryRepository */
    private $repository;

    public function __construct()
    {
        $this->repository = app(CategoryRepository::class);
    }

    /**
     * Получение коллекции с фильтрами, условиями, поиском и пагинацией
     *
     * @param array $parameters
     * @return LengthAwarePaginator|Collection
     */
    public function getList(array $parameters)
    {
        $data['filters'] = $parameters['filters'] ?? [];
        $data['conditions'] = $parameters['conditions'] ?? [];
        $data['search'] = (string)($parameters['search'] ?? '');
        return $this->repository->getList($data);
    }

    /**
     * Получение модели по id
     *
     * @param array $fields
     * @return Model|null
     */
    public function getItem(array $fields): ?Model
    {
        return $this->repository->getItem($fields);
    }

    /**
     * Изменение или создание модели
     *
     * @param array $inputData
     * @param int $item_id
     * @return ?Category
     */
    public function updateOrCreateItem(array $inputData, int $item_id = 0): ?Category
    {
        $item = $this->getItem(['id'=>$item_id]) ?? new Category();
        if($item){
            $item->fill($inputData);
            $item->save();
        }
        return $item;
    }

    /**
     * Удаление модели по id
     *
     * @param int $id
     * @return bool
     */
    public function deleteItem(int $id): bool
    {
        return Category::destroy($id);
    }
}
