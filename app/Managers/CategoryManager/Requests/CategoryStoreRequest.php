<?php

namespace App\Managers\CategoryManager\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;


class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_name'  =>  'required|string|min:1|max:255',
        ];
    }

    /**
     * Configure the validator instance.
     * Дополнительная валидация.
     * @param Validator $validator
     * @return  boolean|void
     */
//    public function withValidator(Validator $validator)
//   {
//        $validator->after(function ($validator) {
//            // Дополнительная проверка
//            if (rule) {
//                return $validator->errors()->add(
//                    'field',
//                    "Дйствие запрещено..."
//                );
//            }
//
//           return true;
//        });
//    }
}
