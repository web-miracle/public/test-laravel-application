<?php

namespace App\Managers\CategoryManager\Requests;

use App\Managers\CategoryManager\Repositories\CategoryRepository;
use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;


class CategoryDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');

        // Проверка категории товара в БД.
        $item = app(CategoryRepository::class)->getItem(['id'=>$id]);
        abort_if(! isset($item), 404, 'Категория товара с данным id в БД отсутствует.');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Configure the validator instance.
     * Дополнительная валидация.
     * @param Validator $validator
     * @return  void
     */
    public function withValidator(Validator $validator)
   {
        $validator->after(function ($validator) {
            $id = $this->route('id');
            $item = Category::has('products')->find($id);
            // Запрет удаления категории при условии, что ей принадлежат товары.
            abort_if($item != null,406, 'Действие запрещено, в категории имеются товары');
        });
    }
}
