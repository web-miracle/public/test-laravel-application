<?php

namespace App\Managers\CategoryManager\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use App\Managers\BaseEntities\BaseRepository;
use App\Models\Category;

class CategoryRepository extends BaseRepository {

    /**
     * Реализация абстрактного метода из BaseRepository.
     * Определение полного имени класса модели, для которой в репозитории будет создан объект.
     * @return string
     */
    protected function getModelName(): string
    {
        return Category::class;
    }

    /**
     * Коллекция с фильтрами, условиями, поиском и пагинацией
     * @param array $data
     * @return LengthAwarePaginator|Collection
     */
    public function getList(array $data)
    {
        return $this->getCloneObject()->newQuery()
            // ЖАДНАЯ ЗАГРУЗКА СВЯЗЕЙ
            //->with()
            // ФИЛЬТРАЦИЯ
            ->when($data['filters'], function ($query, $filters) {
                foreach ($filters as $key => $value) {
                    $query->whereIn($key, $value);
                }
            })
            // ПОИСК
             ->when($data['search'], function ($query, $search) {
                 $query->where('category_name', 'like', '%' . $search . '%');
             })
            // СОРТИРОВКА
            ->orderBy($data['conditions']['sort'] ?? 'id',
                $data['conditions']['direction'] ?? 'desc')
            // ПАГИНАЦИЯ
            ->when(empty($data['conditions']['per_page']), function ($query) {
                return $query->paginate(BaseRepository::PER_PAGE);
            }, function ($query) use ($data) {
                return $query->when(strtolower($data['conditions']['per_page']) == 'all',
                    function ($query) {
                        return $query->get();
                    }, function ($query) use ($data) {
                        return $query->paginate($data['conditions']['per_page']);
                    });
            });
    }

    /**
     * Получение модели исходя из значения поля.
     * @param array $data
     * @return Model|null
     */
    public function getItem(array $data): ?Model
    {
        return $this -> getCloneObject() -> newQuery()
//            -> with([])
            -> where($data)
            -> first();
    }
}
