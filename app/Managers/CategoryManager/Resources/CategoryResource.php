<?php

namespace App\Managers\CategoryManager\Resources;

use App\Managers\BaseEntities\BaseResource;
use Illuminate\Http\Request;


class CategoryResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                => $this->id,
            'category_name'     => $this->category_name,
        ];
    }
}
