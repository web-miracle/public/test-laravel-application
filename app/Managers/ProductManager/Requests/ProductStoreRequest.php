<?php

namespace App\Managers\ProductManager\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;


class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_name'  =>  'required|string|min:1|max:255',
            'price'         =>  'sometimes|integer|min:0|max:4294967295',
            'published'     =>  'sometimes|boolean',
            'category'    =>    'required|array',
            'category.*'    =>  'required|exists:categories,id'
        ];
    }

    /**
     * Дополнительная валидация.
     * @param Validator $validator
     * @return  void
     */
    public function withValidator(Validator $validator)
   {
        $validator->after(function ($validator) {
            // Определение минимального/максимального количества категорий.
            abort_if(count($this->category) < 2,406, 'Количетво категорий должно быть не менее 2.');
            abort_if(count($this->category) > 10,406, 'Количетво категорий должно быть не более 10.');
        });
    }
}
