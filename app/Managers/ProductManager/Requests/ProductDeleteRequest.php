<?php

namespace App\Managers\ProductManager\Requests;

use App\Managers\ProductManager\Repositories\ProductRepository;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;


class ProductDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');

        // Проверка наличия товара в БД.
        $item = app(ProductRepository::class)->getItem(['id'=>$id], true);
        abort_if(! isset($item), 404, 'Позиция товара с данным id в БД отсутствует.');

        // Проверка наличия товара на предмет удаления.
        $item = app(ProductRepository::class)->getItem(['id'=>$id]);
        abort_if(! isset($item), 410, 'Позиция товара с данным id была удалена.');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Configure the validator instance.
     * Дополнительная валидация.
     * @param Validator $validator
     * @return  boolean|void
     */
//    public function withValidator(Validator $validator)
//   {
//        $validator->after(function ($validator) {
//            // Дополнительная проверка
//            if (rule) {
//                return $validator->errors()->add(
//                    'field',
//                    "Дйствие запрещено..."
//                );
//            }
//
//           return true;
//        });
//    }
}
