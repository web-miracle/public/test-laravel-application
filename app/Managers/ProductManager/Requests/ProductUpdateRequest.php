<?php

namespace App\Managers\ProductManager\Requests;

use App\Managers\ProductManager\Repositories\ProductRepository;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;


class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $id = $this->route('id');

        // Проверка наличия товара в БД.
        $item = app(ProductRepository::class)->getItem(['id'=>$id], true);
        abort_if(! isset($item), 404, 'Позиция товара с данным id в БД отсутствует.');

        // Проверка наличия товара на предмет удаления.
        $item = app(ProductRepository::class)->getItem(['id'=>$id]);
        abort_if(! isset($item), 410, 'Позиция товара с данным id была удалена.');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_name'  =>  'sometimes|string|min:1|max:255',
            'price'         =>  'sometimes|nullable|integer|min:0|max:4294967295',
            'published'     =>  'sometimes|boolean',
            'category'    =>    'sometimes|array',
            'category.*'    =>  'required|exists:categories,id'
        ];
    }

    /**
     * Дополнительная валидация.
     * @param Validator $validator
     * @return  void
     */
    public function withValidator(Validator $validator)
   {
        $validator->after(function ($validator) {
            if ($this->has('category')) {
                // Определение минимального/максимального количества категорий.
                abort_if(count($this->category) < 2,406, 'Количетво категорий должно быть не менее 2.');
                abort_if(count($this->category) > 10,406, 'Количетво категорий должно быть не более 10.');
            }
        });
    }
}
