<?php

namespace App\Managers\ProductManager;

use App\Models\Category;
use Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use App\Managers\ProductManager\Repositories\ProductRepository;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductManager {

    /** @var ProductRepository */
    private $repository;

    public function __construct()
    {
        $this->repository = app(ProductRepository::class);
    }

    /**
     * Получение коллекции с фильтрами, условиями, поиском и пагинацией
     *
     * @param array $parameters
     * @return LengthAwarePaginator|Collection
     */
    public function getList(array $parameters)
    {
        $data['filters'] = $parameters['filters'] ?? [];
        $data['conditions'] = $parameters['conditions'] ?? [];
        $data['search'] = (string)($parameters['search'] ?? '');
        return $this->repository->getList($data);
    }

    /**
     * Получение модели по id
     *
     * @param array $fields
     * @param bool $trashed
     * @return Model|null
     */
    public function getItem(array $fields, bool $trashed = false): ?Model
    {
        return $this->repository->getItem($fields, $trashed);
    }

    /**
     * Изменение или создание модели
     *
     * @param array $inputData
     * @param int $item_id
     * @return ?Product
     */
    public function updateOrCreateItem(array $inputData, int $item_id = 0): ?Product
    {
        $item = $this->getItem(['id'=>$item_id]) ?? new Product();
        $categoryIDs = Arr::pull($inputData, 'category');
        if($item){
            $item->fill($inputData);
            $item->save();
            if ($categoryIDs != null) {
                $item->categories()->sync($categoryIDs);
            }
        }
        $item->load('categories');
        return $item;
    }

    /**
     * Удаление модели по id
     *
     * @param int $id
     * @return bool
     */
    public function deleteItem(int $id): bool
    {
        return Product::destroy($id);
    }

    /**
     * Восстановление модели по id
     *
     * @param int $id
     * @return bool
     */
    public function restoreItem(int $id): bool
    {
        $item = $this->getItem(['id'=>$id], true);
        return $item ? $item->restore() : false;
    }
}
