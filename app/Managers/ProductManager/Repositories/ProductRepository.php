<?php

namespace App\Managers\ProductManager\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use App\Managers\BaseEntities\BaseRepository;
use App\Models\Product;

class ProductRepository extends BaseRepository {

    /**
     * Реализация абстрактного метода из BaseRepository.
     * Определение полного имени класса модели, для которой в репозитории будет создан объект.
     * @return string
     */
    protected function getModelName(): string
    {
        return Product::class;
    }

    /**
     * Коллекция с фильтрами, условиями, поиском и пагинацией
     * @param array $data
     * @return LengthAwarePaginator|Collection
     */
    public function getList(array $data)
    {
        return $this->getCloneObject()->newQuery()
            // ЖАДНАЯ ЗАГРУЗКА СВЯЗЕЙ
//            ->with()
            // ФИЛЬТРАЦИЯ
            ->when($data['filters'], function ($query, $filters) {
                foreach ($filters as $key => $value) {
                    // по цене
                    if (strnatcasecmp($key, 'price') == 0) {
                        if (isset($value['min'])) {
                            $query->where('price', '>=', $value['min']);
                        }
                        if (isset($value['max'])) {
                            $query->where('price', '<=', $value['max']);
                        }
                    // по категории
                    }elseif (strnatcasecmp($key, 'category') == 0) {
                        // по ID категории
                        if (!empty($value['id'])) {
                            $query->whereHas('categories', function ($query) use ($value) {
                                $query->whereIn('categories.id', $value['id']);
                            });
                        }
                        // по названию категории
                        if (!empty($value['category_name'])) {
                            $query->whereHas('categories', function ($query) use ($value) {
                                $query->whereIn('category_name', $value['category_name']);
                            });
                        }
                    // по остальным полям табл. products
                    }else {
                        $query->whereIn($key, $value);
                    }
                }
            })
            // ПОИСК
             ->when($data['search'], function ($query, $search) {
                 $query->where('product_name', 'like', '%' . $search . '%')
            		   ->orWhere('price', 'like', '%' . $search . '%');
             })
            // С УДАЛЕННЫМИ ЗАПИСЯМИ
             ->when($data['conditions']['trash'] ?? 0, function ($query) {
                 $query->withTrashed();
             })
            // ТОЛЬКО УДАЛЕННЫЕ ЗАПИСИ
             ->when($data['conditions']['onlyTrash'] ?? false, function ($query) {
                 $query->onlyTrashed();
             })
            // СОРТИРОВКА
            ->orderBy($data['conditions']['sort'] ?? 'id',
                $data['conditions']['direction'] ?? 'desc')
            // ПАГИНАЦИЯ
            ->when(empty($data['conditions']['per_page']), function ($query) {
                return $query->paginate(BaseRepository::PER_PAGE);
            }, function ($query) use ($data) {
                return $query->when(strtolower($data['conditions']['per_page']) == 'all',
                    function ($query) {
                        return $query->get();
                    }, function ($query) use ($data) {
                        return $query->paginate($data['conditions']['per_page']);
                    });
            });
    }

    /**
     * Получение модели исходя из значения поля.
     * @param array $data
     * @param bool $trashed
     * @return Model|null
     */
    public function getItem(array $data, bool $trashed = false): ?Model
    {
        return $this -> getCloneObject() -> newQuery()
            -> when($trashed, function ($query) {
                return $query->withTrashed();
            })
            -> with('categories')
            -> where($data)
            -> first();
    }
}
