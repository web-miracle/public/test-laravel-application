<?php

namespace App\Managers\ProductManager\Resources;

use App\Managers\BaseEntities\BaseResource;
use App\Managers\CategoryManager\Resources\CategoryResourceCollection;
use Illuminate\Http\Request;


class ProductResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            =>  $this->id,
            'product_name'  =>  $this->product_name,
            'price'         =>  $this->price,
            'published'     =>  $this->published,
            'created_at'    =>  $this->created_at,
            'updated_at'    =>  $this->updated_at,
            'deleted_at'    =>  $this->deleted_at,
            'categories'    =>  new CategoryResourceCollection($this -> whenLoaded('categories')),
        ];
    }
}
