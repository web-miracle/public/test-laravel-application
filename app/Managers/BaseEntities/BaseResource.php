<?php

namespace App\Managers\BaseEntities;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource
{

    /** @var array */
    protected $messages;

    public function __construct($resource, $messages = ['Операция выполнена'])
    {
        parent::__construct($resource);

        $this->messages = $messages;
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return parent::toArray($request);
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param Request $request
     * @return array
     */
    public function with($request): array
    {
        return [
            'status' => 'ok',
            'messages' => $this->messages,
        ];
    }
}
