<?php


namespace App\Managers\BaseEntities;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * Количество сущностей на страницу
     * @var int
     */
    const PER_PAGE = 15;

    /**
     * Свойство для хранения объекта.
     * @var Model
     */
    protected $model;

    /**
     * Порождение экземпляра модели и запись его в $model.
     */
    public function __construct()
    {
        $this -> model = app($this -> getModelName());
    }

    /**
     * Метод для хранения названия модели.
     * @return string
     */
    abstract protected function getModelName();

    /**
     * Cоздание клона экземпляра модели $model.
     * @return mixed
     */
    protected function getCloneObject(): mixed
    {
        return clone $this -> model;
    }
}
