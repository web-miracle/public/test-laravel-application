<?php

namespace App\Traits;

trait FileTrait
{
    /**
     * Создание директории
     *
     * @param string $pathToFile
     * @return void
     */
    protected function createDir(string $pathToFile)
    {
        $array = explode('/', $pathToFile);
        array_pop($array);
        $dir = implode('/',$array);
        if (!is_dir($dir)) {
            mkdir($dir);
        }
    }
}
