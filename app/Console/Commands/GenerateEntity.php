<?php

namespace App\Console\Commands;

use App\Traits\FileTrait;
use Illuminate\Console\Command;

/**
 * Создание новой сущности
 * Модель, миграция, контроллер, менеджер, репозиторий, реквесты, ресурсы, политика
 */
class GenerateEntity extends Command
{
    use FileTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:entity {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Генерация новой сущности';

    /**
     * Имя новой сущности
     *
     * @var string
     */
    private $entityName;

    /**
     * Путь к новой сущности
     *
     * @var string
     */
    private $managerPath;

    /**
     * Список файлов для генерации
     *
     * [
     *  'from' => <pathToSourceFile>,
     *  'to'   => <pathToTargetFile>,
     * ]
     * @var array
     */
    protected $stubs = [];

    /**
     * Список замен в файлах генерации
     *
     * <key> => <value>,
     * @var array
     */
    protected $stubsReplaces = [];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->info($this->description);

        $this->entityName = ucfirst(trim($this->argument('name')));
        $this->managerPath = 'App\\Managers\\' . $this->entityName . 'Manager';

        // Генерация контроллера, менеджера, репозитория
        $this->generate();

        // Генерация модели
        $this->call('make:model',[
            'name' => $this->entityName,
            '--migration' => 'true',
        ]);

        // Генерация ресурсов
        $this->call('make:resource',[
            'name' => $this->managerPath . '\\Resources\\' . $this->entityName . 'Resource'
        ]);
        $this->call('make:resource',[
                'name' => $this->managerPath . '\\Resources\\' . $this->entityName . 'ResourceCollection'
        ]);

        // Генерация политик
        $this->call('make:policy', [
            'name' => $this->managerPath . '\\Policies\\' . $this->entityName . 'Policy',
            '--model' => $this->entityName,
        ]);

        // Генерация файлов проверок входных запросов
        $requesTypes = [
            'StoreRequest',
            'UpdateRequest',
            'DeleteRequest',
            'RestoreRequest'
        ];
        foreach ($requesTypes as $type) {
            $this->call('make:request',
                ['name' => $this->managerPath . '\\Requests\\' . $this->entityName . $type,]);
        }
    }

    /**
     * Генерируем файлы по шаблону
     *
     * @return $this
     */
    protected function generate(): static
    {
        $this->initStubs()
            ->initStubsReplaces()
            ->generateFiles();

        return $this;
    }

    /**
     * Инициализация списка создаваемых файлов
     *
     * @return $this
     */
    protected function initStubs(): static
    {
        // Controller
        $this->stubs[] = [
            'from' => base_path('stubs/CustomStubs/Controller.stub'),
            'to' => app_path("Http/Controllers/{$this->entityName}Controller.php"),
        ];
        // Manager
        $this->stubs[] = [
            'from' => base_path('stubs/CustomStubs/Manager.stub'),
            'to' => app_path("Managers/{$this->entityName}Manager/{$this->entityName}Manager.php"),
        ];
        // Repository
        $this->stubs[] = [
            'from' => base_path('stubs/CustomStubs/Repository.stub'),
            'to' => app_path("Managers/{$this->entityName}Manager/Repositories/{$this->entityName}Repository.php"),
        ];

        return $this;
    }

    /**
     * Инициализация списка замен
     *
     * @return $this
     */
    protected function initStubsReplaces(): static
    {
        $this->stubsReplaces['%%EntityName%%'] = $this->entityName;
        $this->stubsReplaces['%%ManagerPath%%'] = $this->managerPath;

        return $this;
    }

    /**
     * Берем каждый исходный файл, кроме тех для которых целевой существует
     * Проводим в нем замены по списку
     * Сохраняем результат в целевой файл
     *
     * @return $this
     */
    protected function generateFiles(): static
    {
        foreach ($this->stubs as $stub) {
            if (is_file($stub['to'])) {
                $this->line("Файл существует: {$stub['to']};");
                continue;
            }

            $content = file_get_contents($stub['from']);
            foreach ($this->stubsReplaces as $search => $replace) {
                $content = str_replace($search, $replace, $content);
            }

            $this->createDir($stub['to']);

            file_put_contents($stub['to'], $content);

            $this->info("Файл сгенерирован: ");
            $this->line($stub['to']);
        }

        return $this;
    }
}
