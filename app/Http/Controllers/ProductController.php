<?php

namespace App\Http\Controllers;

use App\Managers\ProductManager\Requests\ProductStoreRequest;
use App\Managers\ProductManager\Requests\ProductUpdateRequest;
use App\Managers\ProductManager\Requests\ProductDeleteRequest;
use App\Managers\ProductManager\Requests\ProductRestoreRequest;
use App\Managers\ProductManager\Resources\ProductResource;
use App\Managers\ProductManager\Resources\ProductResourceCollection;
use App\Managers\ProductManager\ProductManager;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller {

    public function __construct() {
        $this->manager = app(ProductManager::class);
        $this->resource = ProductResource::class;
        $this->resourceCollection = ProductResourceCollection::class;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return JsonResponse
     */
    public function store(ProductStoreRequest $request) {
        $data = $request->all();
        $item = $this->manager->updateOrCreateItem($data);

        return $item ?
            new $this->resource($item, ['Запись создана']) :
            $this->sendError();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(ProductUpdateRequest $request, int $id) {
        $data = $request->all();
        $item = $this->manager->updateOrCreateItem($data, $id);

        return $item ?
            new $this->resource($item, ['Запись изменена']) :
            $this->sendError();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductDeleteRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(ProductDeleteRequest $request, int $id): JsonResponse
    {
        $status = $this->manager->deleteItem($id);

        return $status ?
            $this->sendResponse([], ['Данные удалены']) :
            $this->sendError(['Данные не найдены'], 404);
    }

    /**
     * Restore item
     *
     * @param ProductRestoreRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function restore(ProductRestoreRequest $request, int $id): JsonResponse
    {
        $status = $this->manager->restoreItem($id);

        return $status ?
            $this->sendResponse([], ['Данные восстановлены']) :
            $this->sendError(['Данные не найдены', 404]);
    }
}
