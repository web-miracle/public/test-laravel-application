<?php

namespace App\Http\Controllers;

use App\Managers\CategoryManager\Requests\CategoryStoreRequest;
use App\Managers\CategoryManager\Requests\CategoryUpdateRequest;
use App\Managers\CategoryManager\Requests\CategoryDeleteRequest;
use App\Managers\CategoryManager\Requests\CategoryRestoreRequest;
use App\Managers\CategoryManager\Resources\CategoryResource;
use App\Managers\CategoryManager\Resources\CategoryResourceCollection;
use App\Managers\CategoryManager\CategoryManager;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller {

    public function __construct() {
        $this->manager = app(CategoryManager::class);
        $this->resource = CategoryResource::class;
        $this->resourceCollection = CategoryResourceCollection::class;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryStoreRequest $request
     * @return JsonResponse
     */
    public function store(CategoryStoreRequest $request) {
        $data = $request->all();
        $item = $this->manager->updateOrCreateItem($data);

        return $item ?
            new $this->resource($item, ['Запись создана']) :
            $this->sendError();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CategoryUpdateRequest $request, int $id) {
        $data = $request->all();
        $item = $this->manager->updateOrCreateItem($data, $id);

        return $item ?
            new $this->resource($item, ['Запись изменена']) :
            $this->sendError();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CategoryDeleteRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(CategoryDeleteRequest $request, int $id): JsonResponse
    {
        $status = $this->manager->deleteItem($id);

        return $status ?
            $this->sendResponse([], ['Данные удалены']) :
            $this->sendError(['Данные не найдены'], 404);
    }
}
