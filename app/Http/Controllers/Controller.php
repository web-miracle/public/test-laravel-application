<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $messages = [
        'show' => [
            'success' => 'Данные получены',
            'error' => 'Данные не существуют или были удалены',
        ],
        'destroy' => [
            'success' => 'Данные удалены',
            'error' => 'Данные не найдены',
        ],
        'restore' => [
            'success' => 'Данные восстановлены',
            'error' => 'Данные не найдены',
        ],
    ];

    /** @var Model */
    protected $model;

    /** @var object */
    protected object $manager;

    /** @var string */
    protected string $resource;

    /** @var string */
    protected string $resourceCollection;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $collection = $this->manager->getList($data) ?? collect();

        return new $this->resourceCollection($collection);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $item = $this->manager->getItem(['id'=>$id]);

        return $item ?
            new $this->resource($item, [$this->messages['show']['success']]) :
            $this->sendError([$this->messages['show']['error']]);
    }

    /**
     * Формирование ответа
     *
     * @param ?array $data
     * @param array $messages
     * @return JsonResponse
     */
    protected function sendResponse(?array $data = [], array $messages = []): JsonResponse
    {
        $response = [
            'status' => 'ok',
            'messages' => $messages,
            'data' => $data,
        ];

        return response()->json($response, 200);
    }

    /**
     * Формирование ответа ошибки
     *
     * @param array $messages
     * @param int $code
     * @return JsonResponse
     */
    protected function sendError(array $messages = ['Произошла ошибка'], int $code = 404): JsonResponse
    {
        $response = [
            'status' => 'error',
            'messages' => $messages,
        ];

        return response()->json($response, $code);
    }
}
