<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     * @var string
     */
//     protected $table = '';

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
//     public $timestamps = false;

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'published'     => 'boolean',
        'created_at'    => 'string',
        'updated_at'    => 'string',
        'deleted_at'    => 'string'
    ];

    /**
     * The model's default values for attributes.
     * @var array
     */
    protected $attributes = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
//    protected $appends = [];

    /**
     * Категории к которым относится товар
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category', 'product_id', 'category_id')
            ->withPivot('created_at');
    }
}
