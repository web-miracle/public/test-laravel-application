<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductCategory extends Model
{
    use HasFactory;
//    use SoftDeletes;

    /**
     * The table associated with the model.
     * @var string
     */
     protected $table = 'product_category';

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
     public $timestamps = false;

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'created_at' => 'string'
    ];

    /**
     * The model's default values for attributes.
     * @var array
     */
    protected $attributes = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
//    protected $appends = [];

    /**
     * Категория
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * Товар
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
